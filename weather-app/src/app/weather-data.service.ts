import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherDataService {

  constructor(private http:HttpClient) { }
  data:any;
  getWeather():Observable<any>
  {
    return this.http.get<any>('https://samples.openweathermap.org/data/2.5/forecast/hourly?q=London,us&appid=b6907d289e10d714a6e88b30761fae22');
  }
  setCurrentWeather(data)
  {
    this.data = data;
  }
  getCurrentWeather()
  {
    return this.data;
  }
}
