import { Component, OnInit } from '@angular/core';
import { WeatherDataService } from "../weather-data.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {
  
  constructor(private _getdata :WeatherDataService,private router : Router) { 
    
  }
  values=[]
  weather_detail=[]
  ngOnInit() {
    this._getdata.getWeather().subscribe(
      data => {
        this.values=data.list;
        this.weather_detail = data.weather;
        console.log("hello")
    
        console.log(this.values);
      }
    );
  }
onSelect(e)
{
    this._getdata.setCurrentWeather(e);
    this.router.navigateByUrl("/day-detail");
}
  
}

