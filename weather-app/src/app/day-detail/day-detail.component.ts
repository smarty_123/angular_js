import { Component, OnInit } from '@angular/core';
import {Router } from "@angular/router";
import { WeatherDataService } from '../weather-data.service';

@Component({
  selector: 'app-day-detail',
  templateUrl: './day-detail.component.html',
  styleUrls: ['./day-detail.component.css']
})


export class DayDetailComponent implements OnInit {

  constructor(private _getdata :WeatherDataService, private router : Router) { }
  
  values=[]
  ngOnInit() {
    this.values = this._getdata.getCurrentWeather();
    console.log("values.list");
    
  }
  onclick()
  {
    this.router.navigateByUrl("/weather");
  }
}
