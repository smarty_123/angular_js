import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from "@angular/router";
import { DayComponent } from './day/day.component';
import { DayDetailComponent } from './day-detail/day-detail.component';

const routes : Routes = [
  {path: '', component: DayComponent, pathMatch: 'full'},
  {path: 'day', component:DayComponent},
  {path:'day-detail',component:DayDetailComponent}
] 

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
export const routingComponents = [DayComponent,DayDetailComponent];